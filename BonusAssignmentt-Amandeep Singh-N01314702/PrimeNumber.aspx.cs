﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignmentt_Amandeep_Singh_N01314702
{
    public partial class PrimeNumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Check_PrimeNumber(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            int userInput = int.Parse(userInputNumber.Text);

            bool flag = false;

            for (int i = userInput - 1; i > 1; i--)
            {
                if (userInput % i == 0)
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                result.InnerHtml = "<strong> Result:</strong> \"" + userInput + "\" is a Prime Number";
            }
            else
            {
                result.InnerHtml = "<strong> Result:</strong> \"" + userInput + "\" is not a Prime Number";
            }
        }

    }
}