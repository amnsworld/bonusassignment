﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignmentt_Amandeep_Singh_N01314702
{
    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Check_Quadrant_Value(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            double xaxisValue = Convert.ToDouble(userInputXAxis.Text);
            double yaxisValue = Convert.ToDouble(userInputYAxis.Text);

            if (xaxisValue > 0 && yaxisValue > 0)
            {
                result.InnerHtml = "<strong>Result:</strong> (" + userInputXAxis.Text +"," +userInputYAxis.Text  + ") lies in Quadrant 1";
            }
            else if (xaxisValue < 0 && yaxisValue > 0)
            {
                result.InnerHtml = "<strong>Result:</strong> (" + userInputXAxis.Text + "," + userInputYAxis.Text + ") lies in Quadrant 2";
            }
            else if(xaxisValue < 0 && yaxisValue < 0)
            {
                result.InnerHtml = "<strong>Result:</strong> (" + userInputXAxis.Text + "," + userInputYAxis.Text + ") lies in Quadrant 3";
            }
            else if (xaxisValue > 0 && yaxisValue < 0)
            {
                result.InnerHtml = "<strong>Result:</strong> (" + userInputXAxis.Text + "," + userInputYAxis.Text + ") lies in Quadrant 4";
            }
        }

        protected void userInputXAxisCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(userInputXAxis.Text) == 0)
            {
                args.IsValid = false;
            }
        }
        protected void userInputYAxisCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(userInputYAxis.Text) == 0)
            {
                args.IsValid = false;
            }
        }
        
    }
}