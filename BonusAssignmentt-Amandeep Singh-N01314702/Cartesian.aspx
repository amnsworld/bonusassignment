﻿<%@ Page Title="Cartesian Smartesian" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="BonusAssignmentt_Amandeep_Singh_N01314702.Cartesian" %>
<asp:Content ID="Question" ContentPlaceHolderID="Content1" runat="server">
    <div>
        <br />
        <br />
        Given input of two integers, create a server click function which prints a message to a blank div
        element of which quadrant the co-ordinate would fall. Validate your inputs to assume that x and
        y are non-zero (for the sake of simplicity).
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="userInputControls" ContentPlaceHolderID="Content2" runat="server">
    <div>
        <asp:Label runat="server">Enter x-axis value: </asp:Label>
        <asp:TextBox runat="server" ID="userInputXAxis"></asp:TextBox>
        <asp:RequiredFieldValidator ID="userInputXAxisRequired" runat="server" ControlToValidate="userInputXAxis" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="userInputXAxisRegularExpression" runat="server" ControlToValidate="userInputXAxis" ErrorMessage="Please enter a valid number to proceed." ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <asp:CustomValidator ID="userInputXAxisCustomValidator" runat="server" ControlToValidate="userInputXAxis" OnServerValidate="userInputXAxisCustomValidator_ServerValidate" ErrorMessage="X-Axis value must be a non-zero number." ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
        <br />
        <asp:Label runat="server">Enter y-axis value: </asp:Label>
        <asp:TextBox runat="server" ID="userInputYAxis"></asp:TextBox>
        <asp:RequiredFieldValidator ID="userInputYAxisRequired" runat="server" ControlToValidate="userInputYAxis" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="userInputYAxisRegularExpression" runat="server" ControlToValidate="userInputYAxis" ErrorMessage="Please enter a valid number to proceed." ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <asp:CustomValidator ID="userInputYAxisCustomValidator" runat="server" ControlToValidate="userInputYAxis" OnServerValidate="userInputYAxisCustomValidator_ServerValidate" ErrorMessage="Y-Axis value must be a non-zero number." ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
        <br />
        <br />
        <asp:Button ID="submitUserInputNumber" Text="Check Quadrant Value?" runat="server" OnClick="Check_Quadrant_Value"/>
    </div>
</asp:Content>
<asp:Content ID="resultArea" ContentPlaceHolderID="Content3" runat="server">
    <div id="result" runat="server"></div>
</asp:Content>