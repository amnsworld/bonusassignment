﻿<%@ Page Title="String Bling" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="BonusAssignmentt_Amandeep_Singh_N01314702.Palindrome" %>
<asp:Content ID="Question" ContentPlaceHolderID="Content1" runat="server">
    <div> 
        <br />
        <br />
        Write an input which checks that the input is a string of characters. Determine if a word is a palindrome or not. 
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="userInputControls" ContentPlaceHolderID="Content2" runat="server">
    <div>
        <asp:Label runat="server">Enter string to be checked. </asp:Label>
        <asp:TextBox runat="server" ID="userInputString"></asp:TextBox>
        <asp:RequiredFieldValidator ID="userInputStringRequired" runat="server" ControlToValidate="userInputString" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="userInputStringRegularExpression" runat="server" ControlToValidate="userInputString" ErrorMessage="Please enter a valid string to proceed." ValidationExpression="[a-zA-Z ]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Button ID="submitUserInputString" Text="Check string for Palidrome?" runat="server" OnClick="Check_Palindrome"/>
    </div>
</asp:Content>
<asp:Content ID="resultArea" ContentPlaceHolderID="Content3" runat="server">
    <div id="result" runat="server"></div>
</asp:Content>
