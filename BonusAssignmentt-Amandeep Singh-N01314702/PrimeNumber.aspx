﻿<%@ Page Title="Divisible Sizzable" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrimeNumber.aspx.cs" Inherits="BonusAssignmentt_Amandeep_Singh_N01314702.PrimeNumber" %>
<asp:Content ID="Question" ContentPlaceHolderID="Content1" runat="server">
    <div>
        <br />
        <br />
        Given a positive integer input, write a server click function which prints a message to a blank div. The message will determine if a number is prime or not 
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="userInputControls" ContentPlaceHolderID="Content2" runat="server">
    <div>
        <asp:Label runat="server">Enter number to be checked for Prime Number. </asp:Label>
        <asp:TextBox runat="server" ID="userInputNumber"></asp:TextBox>
        <asp:RequiredFieldValidator ID="userInputNumberRequired" runat="server" ControlToValidate="userInputNumber" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="userInputNumberRegularExpression" runat="server" ControlToValidate="userInputNumber" ErrorMessage="Please enter a valid number to proceed." ValidationExpression="[0-9]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Button ID="submitUserInputNumber" Text="Check number for Prime Number?" runat="server" OnClick="Check_PrimeNumber"/>
    </div>
</asp:Content>
<asp:Content ID="resultArea" ContentPlaceHolderID="Content3" runat="server">
    <div id="result" runat="server"></div>
</asp:Content>